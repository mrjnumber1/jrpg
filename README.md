### whats this?
this is just a simple self done c11 project that makes a jrpg project. it's mainly educational and strictly for fun. 
### components
1. jlib - the library with the code for storing/reading the game's data files, and its code for making text boxes, importing assets, etc)
2. jcreate - the executable for making the rpg in
3. jplay - the executable for playing the rpg 
4. test - the executable for verifying the code used in jlib's standard library
### installation
compiling simply requires make, and libcheck is required if you intend to run the selftesting application. see [libcheck documentation](https://libcheck.github.io/check/web/install.html) for more info

