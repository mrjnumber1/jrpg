#pragma once

#include "jlib.h"

typedef enum JLogLevel { LOG_TRACE, LOG_DEBUG, LOG_INFO, LOG_WARN, LOG_ERROR, LOG_PANIC, LOG_DISABLE } JLogLevel;

#define JLOG_FILENAME "jlog.txt"


void JLog_Log(JLogLevel level, const Char msg[], ALLOC_MARK_PARAMS);
#define JLOG_TRACE(msg) JLog_Log(LOG_TRACE, (msg), ALLOC_MARK)
#define JLOG_DEBUG(msg) JLog_Log(LOG_DEBUG, (msg), ALLOC_MARK)
#define JLOG_INFO(msg) JLog_Log(LOG_INFO, (msg), ALLOC_MARK)
#define JLOG_WARN(msg) JLog_Log(LOG_WARN, (msg), ALLOC_MARK)
#define JLOG_ERROR(msg) JLog_Log(LOG_ERROR, (msg), ALLOC_MARK)
#define JLOG_PANIC(msg) JLog_Log(LOG_PANIC, (msg), ALLOC_MARK)
