#pragma once
#include <jlib.h>

/// type with non-owning and read-only view to a string (analogous to std::string_view)

/// Create view from array
JStr* JStr_New(Size length, const Char text[PARAM_ELEMENTS(length)]);
/// Create view from hard coded string
#define JSTR_NEWCONSTSTR(str) JStr_New(sizeof(str)-1, str)
/// Create view of JSB
JStr* JStr_NewJSB(const JSBuf* jsb);
/// Create view of src from [offset, offset+count)
JStr* JStr_NewSubstr(const JStr* src, Size offset, Size count);
/// Destroy view
void JStr_Delete(JStr** this_p);

Size JStr_Length(const JStr* this);
Char* JStr_Data(const JStr* this);
Char JStr_At(const JStr* this, Size index);
Char JStr_AtStart(const JStr* this);
Char JStr_AtEnd(const JStr* this);

Size JStr_RemoveFront(JStr* this, Size count);
Size JStr_Remove(JStr* this, Size count);
Size JStr_Sub(JStr* this, Size offset, Size count);
Size JStr_Trim(JStr* this);
Size JStr_RTrim(JStr* this);
Size JStr_LTrim(JStr* this);

Size JStr_CopyToJSB(const JStr* this, JSBuf* jsb);

Bool JStr_MatchStr(const JStr* lhs, const JStr* rhs);
Bool JStr_MatchJSB(const JStr* this, const JSBuf* jsb);
Bool JStr_MatchCStr(const JStr* this, Size length, const Char text[PARAM_ELEMENTS(length)]);
#define JSTR_MATCHCONSTSTR(js, string) JStr_MatchCStr(js, sizeof(string)-1, string)

Bool JStr_StartsWithStr(const JStr* this, const JStr* needle);
Bool JStr_StartsWithChar(const JStr* this, Char n);
Bool JStr_StartsWithJSB(const JStr* this, const JSBuf* needle);
Bool JStr_StartsWithCStr(const JStr* this, Size length, const Char text[PARAM_ELEMENTS(length)]);
#define JSTR_STARTSWITHCONSTSTR(js, string) JStr_StartsWithCStr(js, sizeof(string)-1, string)

Bool JStr_EndsWithStr(const JStr* this, const JStr* needle);
Bool JStr_EndsWithChar(const JStr* this, Char n);
Bool JStr_EndsWithJSB(const JStr* this, const JSBuf* needle);
Bool JStr_EndsWithCStr(const JStr* this, Size length, const Char text[PARAM_ELEMENTS(length)]);
#define JSTR_ENDSWITHCONSTSTR(js, string) JStr_EndsWithCStr(js, sizeof(string)-1, string)


Bool JStr_ContainsStr(const JStr* this, const JStr* needle);
Bool JStr_ContainsChar(const JStr* this, Char n);
Bool JStr_ContainsJSB(const JStr* this, const JSBuf* needle);
Bool JStr_ContainsCStr(const JStr* this, Size length, const Char text[PARAM_ELEMENTS(length)]);
#define JSTR_CONTAINSCONSTSTR(js, string) JStr_EndsWithCStr(js, sizeof(string)-1, string)


Size JStr_FindFirst(const JStr* this, const JStr* needle, Size start);
Size JStr_FindFirstChar(const JStr* this, Char n, Size start);

Size JStr_FindLast(const JStr* this, const JStr* needle, Size start);
Size JStr_FindLastChar(const JStr* this, Char n, Size start);

Size JStr_FindFirstOf(const JStr* this, const JStr* whitelist, Size start);
Size JStr_FindFirstNotOf(const JStr* this, const JStr* blacklist, Size start);
Size JStr_FindLastOf(const JStr* this, const JStr* whitelist, Size end);
Size JStr_FindLastNotOf(const JStr* this, const JStr* blacklist, Size end);

F32 JStr_ParseF32(const JStr* str);
F64 JStr_ParseF64(const JStr* str);
I32 JStr_ParseI32(const JStr* str, U8 base);
U32 JStr_ParseU32(const JStr* str, U8 base);
I64 JStr_ParseI64(const JStr* str, U8 base);
U64 JStr_ParseU64(const JStr* str, U8 base);
