#pragma once

#define CONCAT(a, b) a##b
#define INDIRECT_CONCAT(a, b) CONCAT(a, b)
#define INTERNAL_VAR(n) CONCAT(CONCAT(__COUNTER__, n), __LINE__)

#define QUOTE(a) #a
#define EXPAND_AND_QUOTE(a) QUOTE(a)

#undef NULL
#ifdef nullptr
#	define NULL nullptr
#else
#	define NULL ((void*)0)
#endif

#ifndef alignas
#	define alignas _Alignas
#	define alignof _Alignof 
#endif

#ifndef static_assert
#	define static_assert _Static_assert
#endif
#define STATIC_ASSERT static_assert

#if __GNUC__
#	define DEBUGTRAP __builtin_trap()
#elif _MSC_VER
#	define DEBUGTRAP __debugbreak()
#else
#	define DEBUGTRAP *(volatile int*)0 = 0
#endif
#ifndef typeof
#	define TYPEOF __typeof__
#else
#	define TYPEOF typeof
#endif

#ifdef DEBUG
#	define ASSERT(cond) if (!(cond)) DEBUGTRAP
#else
#	define ASSERT(cond) ((void)((cond) || (JLog_Log(LOG_PANIC, "Assert Fail: " QUOTE(cond), ALLOC_MARK), 0)))
#endif

#define BUILD_BUG_ON_ZERO(constant_expr) (sizeof(struct { int:-!!(constant_expr); }))

#define STATIC_ERROR_STATEMENT(msg) __attribute__((__error__(msg)))
#define STATIC_WARNING_STATEMENT(msg) __attribute__((__warning__(msg)))

/// Modifers meant to rename keywords that are confusing with their normal name

/// Function is not usable outside of this c file
#define FUNC_PRIVATE static
/// Parameter is at least n elements long, n being a prior parameter
#define PARAM_ELEMENTS(n) static n

/// Operator & made illegal, in our case this means no delete func should be callable on a varaible
#define VAR_NOALIAS register
/// Variable must be compile time constant
#ifdef constexpr
#	define VAR_CONSTEVAL constexpr
#else
#	define VAR_CONSTEVAL static const
#endif

#define SAME_TYPE(a, b) __builtin_types_compatible_p(TYPEOF(a), TYPEOF(b))
#define IS_ARRAY(p) SAME_TYPE((p), &(p)[0])
#define ARRAY_SIZE(a) (sizeof(a)/sizeof((a)[0]) + BUILD_BUG_ON_ZERO(IS_ARRAY(a)))

#define ALLOC_MARK __FILE__, __LINE__, __func__
#define ALLOC_MARK_PARAMS const Char file[], const Size line, const Char func[]
#define ALLOC_MARK_VARS file, line, func

