#pragma once

#include "jlib.h"

void JSys_Puts(Size length, const Char msg[PARAM_ELEMENTS(length)]);


FUNCATTR_NORETURN void JSys_Abort(void);


