#pragma once


STATIC_ASSERT(sizeof(void*) == 8, "project only available for 64bit im lazy sorry");

typedef char Char;
typedef signed char I8;
typedef unsigned char U8;
typedef short I16;
typedef unsigned short U16;
typedef int I32;
typedef unsigned int U32;
typedef long long I64;
typedef unsigned long long U64;
typedef float F32;
typedef double F64;
typedef bool Bool;
typedef void Void;
typedef U64 Size;
typedef I64 SSize;
typedef Size UPtr;
typedef SSize Ptr;
typedef U8 Byte;
typedef I8 SByte;

#ifndef CHAR_BIT
#	define CHAR_BIT 8
#endif

STATIC_ASSERT(sizeof(U8) == 1);
STATIC_ASSERT(sizeof(U16) == 2);
STATIC_ASSERT(sizeof(U32) == 4);
STATIC_ASSERT(sizeof(U64) == 8);
STATIC_ASSERT(sizeof(Size) == 8);

#define I8_MAX ((I8)(0x7F))
#define I8_MIN ((I8)(-1-I8_MAX))
#define U8_MAX ((U8)(0xFF)) 

#define I16_MAX ((I16)(0x7FFF)) 
#define I16_MIN ((I16)(-1-I16_MAX)) 
#define U16_MAX ((U16)(0xFFFF))

#define I32_MAX ((I32)(0x7FFFFFFF))
#define I32_MIN ((I32)(-1-I32_MAX)) 
#define U32_MAX ((U32)(0xFFFFFFFF))

#define I64_MAX ((I64)(0x7FFFFFFFFFFFFFFF)) 
#define I64_MIN ((I64)(-1-I64_MAX))
#define U64_MAX ((U64)(0xFFFFFFFFFFFFFFFF)) 

#define PTR_MIN I64_MIN
#define PTR_MAX I64_MAX
#define UPTR_MAX U64_MAX
#define SIZE_MAX U64_MAX
#define SSIZE_MIN I64_MIN
#define SSIZE_MAX I64_MAX
#define BYTE_MAX U8_MAX
#define SBYTE_MIN I8_MIN
#define SBYTE_MAX I8_MAX
