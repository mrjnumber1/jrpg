#pragma once

#if __STDC_VERSION__ >= 202000L
#	define FUNCATTR_DEPRECATED [[deprecated]]
#	define FLOWATTR_FALLTHROUGH [[fallthrough]]
#	define FUNCATTR_NODISCARD [[nodiscard]]
#	define VARATTR_MAYBEUNUSED [[maybe_unused]]
#	define FUNCATTR_NORETURN [[noreturn]]
#	define FUNCATTR_UNSEQUENCED [[unsequenced]]
#	define FUNCATTR_REPRODUCIBLE [[reproducible]]
#else
#	define FUNCATTR_DEPRECATED __attribute__((deprecated))
#	define FLOWATTR_FALLTHROUGH __attribute__((fallthrough))
#	define FUNCATTR_NODISCARD __attribute__((warn_unused_result))
#	define VARATTR_MAYBE_UNUSED __attribute__((unused))
#	define FUNCATTR_NORETURN __attribute__((noreturn))
#	define FUNCATTR_UNSEQUENCED __attribute__((const))
#	define FUNCATTR_REPRODUCIBLE __attribute__((pure))
#endif

