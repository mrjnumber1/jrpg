#pragma once
#include <jlib.h>

/// Growable String Buffer, analogous to cpp std::string

/// Create new jsb
JSBuf* JSBuf_New(void);
JSBuf* JSBuf_NewCapacity(Size capacity);
/// Create new jsb using contents from byte-array
JSBuf* JSBuf_NewArray(Size length, const Char text[PARAM_ELEMENTS(length)]);
/// Create new jsb from substr of old
JSBuf* JSBuf_NewSubstr(const JSBuf* src, Size offset, Size length);
/// Create new clone of src
JSBuf* JSBuf_CopyNew(const JSBuf* src);
/// Create new jsb from compile-time string
#define JSBUF_NEWCONSTSTR(conststr) JSBuf_NewArray(sizeof(conststr), conststr)
/// Destroy jsb
void JSBuf_Delete(JSBuf** this_p);
/// Determine if any data is written to jsb
Bool JSBuf_IsEmpty(const JSBuf* this);
/// Return amount of chars stored in jsb
Size JSBuf_Length(const JSBuf* this);
/// Declare jsb as empty, optionally resetting the array
void JSBuf_Clear(JSBuf* this, Bool reset_contents);
/// Return amount of chars allocated to jsb
Size JSBuf_Capacity(const JSBuf* this);
/// Return character at index
Char JSBuf_At(const JSBuf* this, Size index);
/// Return first character
Char JSBuf_AtStart(const JSBuf* this);
/// Return last character
Char JSBuf_AtEnd(const JSBuf* this);
/// Return pointer to underlying array NOT NULL-TERMINATED 
const Char* JSBuf_Data(const JSBuf* this);
/// Shrink/Grow jsb to requested capacity
Size JSBuf_ForceResize(JSBuf* this, Size requested);
/// Oprtionally grow jsb to requested if it's bigger, returning if it grew
Bool JSBuf_Reserve(JSBuf* this, Size requested);
/// Optionally shrink jsb capacity to match (near) length, returning if it shrunk
Bool JSBuf_ShrinkToFit(JSBuf* this);
/// Change jsb to single ch of num times, returning new length
Size JSBuf_SetToChar(JSBuf* this, Size num, Char ch);
/// Change jsb to text, returning new length
Size JSBuf_SetToCStr(JSBuf* this, Size length, const Char text[PARAM_ELEMENTS(length)]);
/// Change jsb to CONSTSTR
#define JSBUF_SETTOCONSTSTR(jsb, string) JSBuf_SetToCStr(jsb, sizeof(string)-1, string)
/// Put ch at end of jsb (num times)
Size JSBuf_AppendChar(JSBuf* this, Size num, Char ch);
/// Put text at end of CStr
Size JSBuf_AppendCStr(JSBuf* this, Size length, const Char text[PARAM_ELEMENTS(length)]);
/// Append compile-time string to jsb
#define JSBUF_APPENDCONSTSTR(jsb, conststr) JSBuf_AppendCStr(jsb, sizeof(conststr)-1, conststr)
/// Put num of ch into jsb at offset
Size JSBuf_InsertChar(JSBuf* this, Size num, Char ch, Size offset);
/// Put length-sized text into jsb at offset
Size JSBuf_InsertCStr(JSBuf* this, Size length, const Char text[PARAM_ELEMENTS(length)], Size offset);
/// Put conststr into jsb at offset
#define JSBUF_INSERTCONSTSTR(jsb, conststr, offset) JSBuf_InsertCStr(jsb, sizeof(conststr)-1, conststr, offset)
/// Put num of ch at beginning of jsb
Size JSBuf_PrependChar(JSBuf* this, Size num, Char ch);
/// Put length-sized text at beginning of jsb
Size JSBuf_PrependCStr(JSBuf* this, Size length, const Char text[PARAM_ELEMENTS(length)]);
/// Put conststr at start of jsb
#define JSBUF_PREPENDCONSTSTR(jsb, conststr) JSBuf_PrependCStr(jsb, sizeof(conststr)-1, conststr)
/// Put ch at the end of jsb
void JSBuf_PushBack(JSBuf* this, Char ch);
/// Put ch at front of jsb
void JSBuf_PushFront(JSBuf* this, Char ch);
/// Get ch off the back of jsb
Char JSBuf_PopBack(JSBuf* this);
/// Get ch off the front of jsb
Char JSBuf_PopFront(JSBuf* this);
/// Erase ch from offset of jsb, returning new size
Size JSBuf_EraseAt(JSBuf* this, Size offset);
/// Erase from [offset, offset+count) of jsb, returning new size
Size JSBuf_EraseCount(JSBuf* this, Size offset, Size count);
/// Erase from start to end from jsb, returning new size
Size JSBuf_EraseRange(JSBuf* this, Size start, Size end);

