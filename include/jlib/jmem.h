#pragma once 
#include "jlib.h"

/// Memory allocation functions... ala calloc/realloc/free
/// Ideally, these are not used much.. if at all. Be careful!

/// Func used by JNEW/JNEW_FAST/JNEWLIST/JNEWLIST_FAST, do not call directly 
void* JMem_Alloc(Size element_count, Size entry_size, bool zeroed, ALLOC_MARK_PARAMS);
/// Func used by JMEM_RESIZE, do not call directly 
void* JMem_Resize(void* ptr, Size bytes, ALLOC_MARK_PARAMS);
/// Func used by JDELETE, do not call directly
void JMem_Delete(void** ptr_p, ALLOC_MARK_PARAMS);

/// calloc a single variable. Usage: Object* this = JNEW(this)
#define JNEW(ptr) JMem_Alloc(1, sizeof(*(ptr)), true, ALLOC_MARK)
/// malloc a single variable. Usage: Object* this = JNEW_FAST(this)
#define JNEW_FAST(ptr) JMem_Alloc(1, sizeof(*(ptr)), false, ALLOC_MARK)
/// calloc count elements of type. Usage: T* array = JNEWLIST(count, T)
#define JNEWLIST(elements, type) JMem_Alloc((elements), sizeof(type), true, ALLOC_MARK)
/// malloc count elements of type. Usage: T* array = JNEWLIST_FAST(count, T)
#define JNEWLIST_FAST(elements, type) JMem_Alloc((elements), sizeof(type), false, ALLOC_MARK)
/// realloc list to new element count. Usage: array = JRESIZE(array, new_elements)
#define JRESIZE(ptr, elements) JMem_Resize((void*)(ptr), (elements)*sizeof(*(ptr)), ALLOC_MARK)
/// free a previously allocated ptr. Usage: JDELETE(&ptr)
#define JDELETE(ptr_p) JMem_Delete((void**)(ptr_p), ALLOC_MARK)
