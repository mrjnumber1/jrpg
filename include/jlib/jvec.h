#pragma once
#include "jlib.h"

/// Create empty JVec
JVecByte* JVecByte_New(void);
/// Create empty JVec of given capacity
JVecByte* JVecByte_NewCapacity(Size capacity);
/// Create JVec, copying data from capacity-sized array
JVecByte* JVecByte_NewArray(Size capacity, const Byte array[PARAM_ELEMENTS(capacity)]);
/// Create JVec by copying from src
JVecByte* JVecByte_CopyNew(const JVecByte* src);
/// Delete JVec
void JVecByte_Destroy(JVecByte** this_p);

/// Get amount of entries the JVec will grow to before re-allocating
Size JVecByte_Capacity(const JVecByte* this);
/// Set the capacity of the array, will extend the list if needed, returning new capacity 
Size JVecByte_Resize(JVecByte* this, Size new_capacity);
/// Get amount of entries in JVec
Size JVecByte_Length(const JVecByte* this);

/// Get value at index
Byte JVecByte_At(const JVecByte* this, Size index);
/// Set value at index
void JVecByte_Set(JVecByte* this, Size index, Byte value);
/// Remove values from [start, length), returning new length
Size JVecByte_RemoveSlice(JVecByte* this, Size start, Size length);
/// Place supplied array at end, returning new length 
Size JVecByte_AppendArray(JVecByte* this, Size length, const Byte array[PARAM_ELEMENTS(length)]);
/// Place supplied array at position, returning new length
Size JVecByte_InsertArray(JVecByte* this, Size position, Size length, const Byte array[PARAM_ELEMENTS(length)]);
/// Place supplied array at front, returning new length 
Size JVecByte_PrependArray(JVecByte* this, Size length, const Byte array[PARAM_ELEMENTS(length)]);
/// Place value to front of vector, returning new length 
Size JVecByte_PushFront(JVecByte* this, Byte value);
/// Place value to back of vector, returning new length
Size JVecByte_PushBack(JVecByte* this, Byte value);
/// Get and remove the value at front of vector
Byte JVecByte_PopFront(JVecByte* this);
/// Get and remove the value at end of vector
Byte JVecByte_PopBack(JVecByte* this);
/// Remove all entries in the vector (actually just sets internal length to zero)
void JVecByte_Clear(JVecByte* this);
/// Find needle in vector
Bool JVecByte_Contains(const JVecByte* this, Byte needle);
/// Find offset of needle in vector (SIZE_MAX if not found)
Size JVecByte_IndexOf(const JVecByte* this, Byte needle);
/// Get value at front of vector
Byte JVecByte_AtStart(const JVecByte* this);
/// Get value at end of vector
Byte JVecByte_AtEnd(const JVecByte* this);
/// Get the first entry, fails if vec empty 
const Byte* JVecByte_ItFirst(JVecByte* this);
/// Get the next entry, returns null if at end 
const Byte* JVecByte_ItNext(JVecByte* this);
/// Get the value stored at iter cursor 
Byte JVecByte_AtCursor(const JVecByte* this);

