#pragma once

#include "prelude.h"

#define JLIB_VERSION_MAJOR 0
#define JLIB_VERSION_MINOR 0
#define JLIB_VERSION_PATCH 1
#define JLIB_VERSION (JLIB_VERSION_MAJOR*1000000 + JLIB_VERSION_MINOR * 1000 + JLIB_VERSION_PATCH)

//typedef struct JVecByte JVecByte;
typedef struct JLog JLog;
typedef struct JSBuf JSBuf;
typedef struct JStr JStr;

#include "jsys.h"
#include "jlog.h"
#include "jsbuf.h"
#include "jstr.h"
#include "jmem.h"
//#include "jvec.h"
