#include "suites.h"

START_TEST(Test_JVec)
{
	const Size DEFAULT_CAP = 3;
	JVecByte* vec = JVecByte_NewCapacity(DEFAULT_CAP);
	ck_assert_uint_ge(JVecByte_Capacity(vec), DEFAULT_CAP);
	ck_assert_uint_eq(JVecByte_Length(vec), 0);

	// TODO: how to ensure it's an assert fail to resize to smaller?
	const Size BIGGER_CAP = 7;
	ck_assert_uint_ge(JVecByte_Resize(vec, BIGGER_CAP), BIGGER_CAP);
	ck_assert_uint_gt(JVecByte_Capacity(vec), DEFAULT_CAP);
	ck_assert_uint_eq(JVecByte_Length(vec), 0);

	const Byte VALUES[] = { 7, 15, 29, 39, 50 };
	const Size VALUES_LENGTH = ARRAY_SIZE(VALUES); 
	Size i = 0;
	for (i = 0; i < VALUES_LENGTH; ++i)
	{
		JVecByte_PushBack(vec, VALUES[i]);
		ck_assert_uint_eq(JVecByte_At(vec, i), VALUES[i]);
		ck_assert_uint_eq(JVecByte_AtEnd(vec), VALUES[i]);
	}
	ck_assert_uint_eq(JVecByte_Length(vec), VALUES_LENGTH);

	i = 0;	
	for (const Byte* it = JVecByte_ItFirst(vec); it; it = JVecByte_ItNext(vec))
		ck_assert_uint_eq(JVecByte_AtCursor(vec), VALUES[i++]);

	JVecByte_Set(vec, 3, VALUES[0]);
	ck_assert_uint_eq(JVecByte_At(vec, 3), VALUES[0]);
	ck_assert_uint_eq(JVecByte_Length(vec), VALUES_LENGTH);
	
	ck_assert_uint_eq(JVecByte_RemoveSlice(vec, 0, VALUES_LENGTH), 0);
	ck_assert_uint_eq(JVecByte_AppendArray(vec, VALUES_LENGTH, VALUES), VALUES_LENGTH);
	ck_assert_uint_eq(JVecByte_AppendArray(vec, VALUES_LENGTH, VALUES), VALUES_LENGTH*2);
	ck_assert_uint_eq(JVecByte_RemoveSlice(vec, VALUES_LENGTH-2, 3), VALUES_LENGTH*2-3);

	JVecByte_Clear(vec);
	ck_assert_uint_eq(JVecByte_Length(vec), 0);
	ck_assert_uint_ge(JVecByte_Capacity(vec), 0);

	ck_assert_uint_eq(JVecByte_InsertArray(vec, 0, VALUES_LENGTH, VALUES), VALUES_LENGTH);
	ck_assert_uint_eq(JVecByte_InsertArray(vec, 3, VALUES_LENGTH, VALUES), VALUES_LENGTH*2);
	for (Size i = 3; i < 3+VALUES_LENGTH; ++i)
		ck_assert_uint_eq(JVecByte_At(vec, i), VALUES[i-3]);
	ck_assert_uint_eq(JVecByte_RemoveSlice(vec, 2, VALUES_LENGTH), VALUES_LENGTH);

	const Byte VALA = 66, VALB = 6;

	ck_assert_uint_eq(JVecByte_PushFront(vec, VALA), VALUES_LENGTH+1);
	ck_assert_uint_eq(JVecByte_PushBack(vec, VALB), VALUES_LENGTH+2);
	ck_assert_uint_eq(JVecByte_AtStart(vec), VALA);
	ck_assert_uint_eq(JVecByte_AtEnd(vec), VALB);
	ck_assert_uint_eq(JVecByte_IndexOf(vec, VALA), 0);
	ck_assert_uint_eq(JVecByte_IndexOf(vec, VALB), JVecByte_Length(vec)-1);
	ck_assert(JVecByte_Contains(vec, VALA));
	ck_assert(JVecByte_Contains(vec, VALB));

	ck_assert_uint_eq(JVecByte_PopFront(vec), VALA);
	ck_assert_uint_eq(JVecByte_Length(vec), VALUES_LENGTH+1);
	ck_assert_uint_eq(JVecByte_IndexOf(vec, VALA), SIZE_MAX);
	ck_assert_uint_eq(JVecByte_IndexOf(vec, VALB), JVecByte_Length(vec)-1);
	ck_assert(!JVecByte_Contains(vec, VALA));
	ck_assert(JVecByte_Contains(vec, VALB));

	ck_assert_uint_eq(JVecByte_PopBack(vec), VALB);
	ck_assert_uint_eq(JVecByte_Length(vec), VALUES_LENGTH);
	ck_assert_uint_eq(JVecByte_IndexOf(vec, VALA), SIZE_MAX); 
	ck_assert_uint_eq(JVecByte_IndexOf(vec, VALB), SIZE_MAX);
	ck_assert(!JVecByte_Contains(vec, VALA));
	ck_assert(!JVecByte_Contains(vec, VALB));



}
END_TEST


SUITE_FUNCTION(JVec)
