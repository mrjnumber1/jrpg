#include "suites.h"
#include <stdlib.h>

typedef Suite* (*SuiteCreateFunc)(void);
typedef struct { const char* name; SuiteCreateFunc create_suite; bool stable; } TestItem;

#define JRPGTEST_ENTRY(class, stable) { QUOTE(class), CONCAT(class, _suite), stable }
#define JRPGTEST_STABLE(class) JRPGTEST_ENTRY(class, true)
#define JRPGTEST_UNSTABLE(class) JRPGTEST_ENTRY(class, false)

static TestItem items[] = {
	//JRPGTEST_UNSTABLE(JLog),
	//JRPGTEST_UNSTABLE(JVec),
	JRPGTEST_UNSTABLE(JSBuf),
	JRPGTEST_UNSTABLE(JStr),
// Keep this null entry at the end!!!
	{0, 0, 0}
};

//#define JRPGTEST_LOG_TXT "jrpg_test.log"
//#define JRPGTEST_LOG_XML "jrpg_test.xml"

int main(int argc, char* argv[PARAM_ELEMENTS(argc)])
{
	SRunner* sr = srunner_create(JSys_suite());
	for (TestItem* item = items; item->name; ++item)
		srunner_add_suite(sr, item->create_suite());
#if JRPGTEST_LOG_TXT	
	srunner_set_log(sr, JRPGTEST_LOG_TXT);
#endif
#if JRPGTEST_LOG_XML
	srunner_set_xml(sr, JRPGTEST_LOG_XML);
#endif
#if __WINDOWS__
	srunner_set_fork_status(sr, CK_NOFORK);
#endif

	srunner_run_all(sr, CK_VERBOSE);

	Size num_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (num_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
