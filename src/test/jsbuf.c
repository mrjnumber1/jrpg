#include "suites.h"

START_TEST(Test_JSBuf)
{
	const Size DEFAULT_CAPACITY = 20;
	JSBuf* str = JSBuf_NewCapacity(DEFAULT_CAPACITY);
	ck_assert(JSBuf_IsEmpty(str));
	ck_assert_uint_eq(JSBuf_Length(str), 0);
	ck_assert_uint_ge(JSBuf_Capacity(str), DEFAULT_CAPACITY);

	for (Size i = 0; i < 10; ++i)
	{
		ck_assert_uint_eq(JSBuf_AppendChar(str, 1, '0'+(Char)i), 1+i);
		ck_assert_uint_eq(JSBuf_Length(str), 1+i);
	}
	ck_assert_mem_eq(JSBuf_Data(str), "0123456789", 10);

	ck_assert_uint_eq(JSBUF_APPENDCONSTSTR(str, "piss"), 14);
	ck_assert_mem_eq(JSBuf_Data(str), "0123456789piss", 14);
	ck_assert_uint_eq(JSBuf_Length(str), 14);
	ck_assert_uint_eq(JSBUF_INSERTCONSTSTR(str, "fairy", 10), 19);
	ck_assert_mem_eq(JSBuf_Data(str), "0123456789fairypiss", 19);
	ck_assert_uint_eq(JSBuf_Length(str), 19);

	ck_assert_uint_eq(JSBuf_InsertChar(str, 1, 'l', 14), 20);
	ck_assert_mem_eq(JSBuf_Data(str), "0123456789fairlypiss", 20);
	ck_assert(!JSBuf_Reserve(str, JSBuf_Capacity(str)-1));
	ck_assert(!JSBuf_Reserve(str, 1));

	const Size BIGGER_CAP = 39;
	ck_assert(JSBuf_Reserve(str, BIGGER_CAP));
	ck_assert_uint_ge(JSBuf_Capacity(str), BIGGER_CAP);
	ck_assert_uint_le(JSBuf_Length(str), BIGGER_CAP);
	ck_assert(JSBuf_ShrinkToFit(str));
	ck_assert(!JSBuf_ShrinkToFit(str));

	ck_assert_uint_eq(JSBuf_Length(str), 20);
	ck_assert_uint_ge(JSBuf_Capacity(str), 20);

	ck_assert_uint_eq(JSBuf_AtEnd(str), 's');
	JSBuf_PushBack(str, 'k');
	ck_assert_uint_eq(JSBuf_Length(str), 21);
	ck_assert_uint_eq(JSBuf_AtEnd(str), 'k');

	ck_assert_uint_eq(JSBuf_AtStart(str), '0');
	JSBuf_PushFront(str, 'x');
	ck_assert_uint_eq(JSBuf_AtStart(str), 'x');
	ck_assert_uint_eq(JSBuf_Length(str), 22);
	ck_assert_uint_eq(JSBuf_PopFront(str), 'x');
	ck_assert_uint_eq(JSBuf_Length(str), 21);
	ck_assert_uint_eq(JSBuf_PopBack(str), 'k');
	ck_assert_uint_eq(JSBuf_Length(str), 20);

	ck_assert_uint_eq(JSBuf_AtStart(str), '0');
	ck_assert_uint_eq(JSBuf_EraseAt(str, 0), 19);
	ck_assert_uint_eq(JSBuf_AtStart(str), '1');
	ck_assert_uint_eq(JSBuf_At(str, 13), 'l');
	ck_assert_uint_eq(JSBuf_EraseAt(str, 13), 18);
	ck_assert_uint_ne(JSBuf_At(str, 13), 'l');
	ck_assert_uint_eq(JSBuf_At(str, 13), 'y');
	ck_assert_uint_eq(JSBuf_AtEnd(str), 's');
	ck_assert_uint_eq(JSBuf_PopBack(str), 's');
	ck_assert_uint_eq(JSBuf_PopBack(str), 's');
	ck_assert_uint_eq(JSBuf_PopBack(str), 'i');
	ck_assert_uint_eq(JSBuf_AtEnd(str), 'p');
	ck_assert_uint_eq(JSBuf_PopBack(str), 'p');

	ck_assert_uint_eq(JSBuf_SetToChar(str, 4, '='), 4);
	ck_assert_mem_eq(JSBuf_Data(str), "====", 4);

	const Char SAMPLE_STR[] = "piss";
	ck_assert_uint_eq(JSBuf_SetToCStr(str, sizeof(SAMPLE_STR)-1, SAMPLE_STR), 4);
	ck_assert_mem_eq(JSBuf_Data(str), SAMPLE_STR, 4);
	ck_assert_uint_eq(JSBUF_SETTOCONSTSTR(str, "fairy"), sizeof("fairy")-1);
	ck_assert_mem_eq(JSBuf_Data(str), "fairy", sizeof("fairy")-1);

	const Char LONGSTR[] = "This is an example";
	const Size LONGSTR_LENGTH = sizeof(LONGSTR)-1;
	ck_assert_uint_eq(JSBuf_SetToCStr(str, LONGSTR_LENGTH, LONGSTR), LONGSTR_LENGTH);
	ck_assert_mem_eq(JSBuf_Data(str), LONGSTR, LONGSTR_LENGTH);

	ck_assert_uint_eq(JSBuf_EraseCount(str, 7, 3), LONGSTR_LENGTH-3);
	ck_assert_mem_eq(JSBuf_Data(str), "This is example", LONGSTR_LENGTH-3);
	ck_assert_uint_eq(JSBuf_EraseAt(str, 7), LONGSTR_LENGTH-4); 
	ck_assert_mem_eq(JSBuf_Data(str), "This isexample", LONGSTR_LENGTH-4);
	ck_assert_uint_eq(JSBuf_EraseAt(str, 4), LONGSTR_LENGTH-5);
	ck_assert_mem_eq(JSBuf_Data(str), "Thisisexample", LONGSTR_LENGTH-5);
	ck_assert_uint_eq(JSBuf_EraseRange(str, 0, 5), LONGSTR_LENGTH-10);
	ck_assert_mem_eq(JSBuf_Data(str), "sexample", LONGSTR_LENGTH-10);
	ck_assert_uint_eq(JSBuf_EraseCount(str, 4, 5), 3);
	ck_assert_mem_eq(JSBuf_Data(str), "sex", 3);

}
END_TEST


SUITE_FUNCTION(JSBuf)
