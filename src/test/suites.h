#pragma once

#include <jlib.h>
#include <check.h>

#define SUITE_PROTOTYPE(class) Suite* CONCAT(class, _suite)(void)

SUITE_PROTOTYPE(JSys);
//SUITE_PROTOTYPE(JLog);
//SUITE_PROTOTYPE(JVec);
SUITE_PROTOTYPE(JSBuf);
SUITE_PROTOTYPE(JStr);

#define SUITE_FUNCTION(class) \
Suite* CONCAT(class, _suite)(void) \
{ \
	Suite* s = suite_create(QUOTE(class)); \
	TCase* tc = tcase_create(QUOTE(class) "_core"); \
	\
	tcase_add_test(tc, CONCAT(Test_, class)); \
	suite_add_tcase(s, tc); \
	return s; \
}

