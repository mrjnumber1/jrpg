#include "jvec.h"

enum JVEC_CONSTANTS {
	JVEC_GROWTH_RATE = 2,
	JVEC_DEFAULT_CAPACITY = 16,
	JVEC_MAX_BYTES = 500*1024*1024, // 500 MB seems sane!
	JVEC_CURSOR_INVALID = SIZE_MAX
};

struct JVecByte {
	Size length, capacity;
	Size cursor; 
	Byte* data;
};

/// Create empty JVec (capacity will be JVEC_DEFAULT_CAPACITY)
JVecByte* JVecByte_New(void)
{
	ASSERT(JVEC_DEFAULT_CAPACITY > 0);
	return JVecByte_NewCapacity(JVEC_DEFAULT_CAPACITY);
}

/// Create empty JVec of given capacity
JVecByte* JVecByte_NewCapacity(Size capacity)
{
	ASSERT(capacity > 0);

	JVecByte* this = JNEW(this);
	this->capacity = capacity;
	this->data = JNEWLIST_FAST(capacity, Byte);
	this->cursor = JVEC_CURSOR_INVALID;
	return this;
}
/// Create JVec, copying data from capacity-sized array
JVecByte* JVecByte_NewArray(Size capacity, const Byte array[PARAM_ELEMENTS(capacity)])
{
	ASSERT(capacity > 0);
	JVecByte* this = JVecByte_NewCapacity(capacity);

	for (Size i = 0; i < capacity; ++i)
		this->data[i] = array[i];
	this->capacity = this->length = capacity;

	return this;
}

JVecByte* JVecByte_CopyNew(const JVecByte* src)
{
	ASSERT(src);
	return JVecByte_NewArray(src->capacity, src->data);
}

void JVecByte_Destroy(JVecByte** this_p)
{
	ASSERT(this_p);
	JVecByte* this = *this_p;
	ASSERT(this);

	this->cursor = this->length = this->capacity = 0;
	JDELETE(&this->data);
	JDELETE(&this);
}

/// Get amount of entries the JVec will grow to this->fore re-allocating
Size JVecByte_Capacity(const JVecByte* this)
{
	ASSERT(this);
	return this->capacity;
}

/// Set the capacity of the array, will extend/shrink the list if needed
Size JVecByte_Resize(JVecByte* this, Size new_capacity)
{
	ASSERT(this);

	this->data = JRESIZE(this->data, new_capacity);
	this->capacity = new_capacity;
	if (this->length > this->capacity)
		this->length = this->capacity;

	return this->capacity;
}

/// Get amount of entries in JVec
Size JVecByte_Length(const JVecByte* this)
{
	ASSERT(this);

	return this->length;
}

/// Get value stored at index
Byte JVecByte_At(const JVecByte* this, Size index)
{
	ASSERT(this);
	ASSERT(index < JVecByte_Length(this));

	return this->data[index];
}

/// Set index to value, cannot over-run this->ffer
void JVecByte_Set(JVecByte* this, Size index, Byte value)
{
	ASSERT(this);
	ASSERT(index < JVecByte_Length(this));

	this->data[index] = value;
}

FUNC_PRIVATE void JVecByte_ensurecapacity(JVecByte* this, Size requested)
{
	ASSERT(this);
	ASSERT(requested < JVEC_MAX_BYTES);

	if (this->capacity < requested)
	{
		Size new_capacity = this->capacity * JVEC_GROWTH_RATE; 
		while (new_capacity < requested)
			new_capacity *= JVEC_GROWTH_RATE;
		ASSERT(new_capacity * sizeof(Byte) < JVEC_MAX_BYTES);

		JVecByte_Resize(this, new_capacity);
	}
}

/// remove elements from [start, length), returning the new length
Size JVecByte_RemoveSlice(JVecByte* this, Size start, Size length)
{
	ASSERT(this);
	ASSERT(length);
	ASSERT(start < this->length);

	for (Size i = start; i < (start+length); ++i)
		this->data[i] = this->data[i+length];

	this->length -= length;
	return this->length;
}

/// apply elements from array, regrowing list if needed, returns the new array length
Size JVecByte_AppendArray(JVecByte* this, Size length, const Byte array[PARAM_ELEMENTS(length)])
{
	ASSERT(this);
	ASSERT(length);

	Size needed_capacity = this->length + length;
	if (needed_capacity > this->capacity) JVecByte_Resize(this, needed_capacity);

	for (Size i = 0; i < length; ++i)
		this->data[this->length + i] = array[i];
	this->length += length;

	return this->length;
}

/// place values from array at position
Size JVecByte_InsertArray(JVecByte* this, Size position, Size length, const Byte array[PARAM_ELEMENTS(length)])
{
	ASSERT(this);
	ASSERT(length);
	ASSERT(position <= this->length);

	Size needed_capacity = this->length + length;
	if (needed_capacity > this->capacity) JVecByte_Resize(this, needed_capacity);

	// shift vec forward length indexes
	for (Size i = this->length; i != position; --i)
		this->data[i + length - 1] = this->data[i - 1];
	for (Size i = 0; i < length; ++i)
		this->data[i + position] = array[i];

	this->length += length;
	return this->length;
}

/// place array at start of vec
Size JVecByte_PrependArray(JVecByte* this, Size length, const Byte array[PARAM_ELEMENTS(length)])
{
	ASSERT(this);
	ASSERT(length);

	Size needed_capacity = this->length + length;
	if (needed_capacity > this->capacity) JVecByte_Resize(this, needed_capacity);

	for (Size i = 0; i < length; ++i)
	{
		this->data[this->length + i] = this->data[length + i];
		this->data[length + i] = this->data[i];
		this->data[i] = array[i];
	}

	this->length += length;
	return this->length;
}

Size JVecByte_PushFront(JVecByte* this, Byte value)
{
	ASSERT(this);
	if (this->length + 1 >= this->capacity)
		JVecByte_ensurecapacity(this, this->capacity + 1);

	for (Size i = 0; i < this->length; ++i)
		this->data[i] = this->data[i + 1];
	this->data[0] = value;	
	this->length++;

	return this->length;
}

Size JVecByte_PushBack(JVecByte* this, Byte value)
{
	ASSERT(this);
	if (this->length == this->capacity)
		JVecByte_ensurecapacity(this, this->capacity+1);

	this->data[this->length] = value;
	this->length++;
	return this->length;
}

Byte JVecByte_PopFront(JVecByte* this)
{
	ASSERT(this);
	ASSERT(this->length);

	Byte value = JVecByte_AtStart(this);

	for (Size i = 0; i < this->length; ++i)
		this->data[i] = this->data[i + 1];
	this->length--;

	return value;
}

Byte JVecByte_PopBack(JVecByte* this)
{
	ASSERT(this);
	ASSERT(this->length);

	this->length--;
	Byte value = this->data[this->length];
	this->data[this->length] = (Byte)0;

	return value;
}

void JVecByte_Clear(JVecByte* this)
{
	ASSERT(this);
	this->length = 0;
}

Bool JVecByte_Contains(const JVecByte* this, Byte needle)
{
	ASSERT(this);
	ASSERT(this->length);

	return JVecByte_IndexOf(this, needle) != SIZE_MAX;
}

/// Find index of an item if it's in the list, returns SIZE_MAX if not found
Size JVecByte_IndexOf(const JVecByte* this, Byte needle)
{
	ASSERT(this);
	ASSERT(this->length);

	for (Size i = 0; i < this->length; ++i)
		if (this->data[i] == needle)
			return i;

	return SIZE_MAX;
}

Byte JVecByte_AtStart(const JVecByte* this)
{
	ASSERT(this);
	ASSERT(this->length);
	return JVecByte_At(this, 0);
}

Byte JVecByte_AtEnd(const JVecByte* this)
{
	ASSERT(this);
	ASSERT(this->length);
	return JVecByte_At(this, this->length - 1);
}

Byte JVecByte_AtCursor(const JVecByte* this)
{
	ASSERT(this);
	ASSERT(this->length);
	ASSERT(this->cursor != JVEC_CURSOR_INVALID);
	ASSERT(this->cursor < this->length);

	return JVecByte_At(this, this->cursor);
}

const Byte* JVecByte_ItFirst(JVecByte* this)
{
	ASSERT(this);
	ASSERT(this->length);

	this->cursor = 0;
	return &this->data[this->cursor];  
}

const Byte* JVecByte_ItNext(JVecByte* this)
{
	ASSERT(this);
	ASSERT(this->length);
	ASSERT(this->cursor != JVEC_CURSOR_INVALID);
	ASSERT(this->cursor < this->length);

	this->cursor++;
	if (this->cursor >= this->length)
	{
		this->cursor = JVEC_CURSOR_INVALID;
		return NULL;
	}

	return &this->data[this->cursor];
}


