#include "jstr.h"

struct JStr {
	Char* ptr;
	Size length;
}

JStr* JStr_New(Size length, const Char text[PARAM_ELEMENTS(length)])
{
	ASSERT(text);
	ASSERT(text[length-1]);

	JStr* this = JNEW_FAST(this);
	this.ptr = text;
	this.length = length;
	return this;
}

/// Create view from hard coded string
#define JSTR_NEWCONSTSTR(str) JStr_New(sizeof(str)-1, str)
/// Create view of JSB
JStr* JStr_NewJSB(const JSBuf* jsb)
{
	ASSERT(jsb);
	return JStr_New(JSBuf_Length(jsb), JSBuf_Data(jsb));
}



// determien if the view is still valid
FUNC_PRIVATE Bool JStr_valid(const JStr* this)
{
	ASSERT(this);
	return this->ptr && this->length > 0;
}


/// Create view of src from [offset, offset+count)
JStr* JStr_NewSubstr(const JStr* src, Size offset, Size count)
{
	ASSERT(JStr_valid(src));
	ASSERT(offset < src->length);
	ASSERT((offset+count-1) <= src->length);
	return JStr_New(src->length-offset-count, &src->ptr[offset];
}

/// Destroy view
void JStr_Delete(JStr** this_p)
{
	ASSERT(this_p);
	JStr* this = *this_p;
	ASSERT(this);

	JDELETE(&this);
}



Size JStr_Length(const JStr* this)
{
	ASSERT(JStr_valid(this));
}

Char* JStr_Data(const JStr* this)
{

	ASSERT(JStr_valid(this));
}

Char JStr_At(const JStr* this, Size index)
{

	ASSERT(JStr_valid(this));
}

Char JStr_AtStart(const JStr* this)
{

	ASSERT(JStr_valid(this));
}

Char JStr_AtEnd(const JStr* this)
{

	ASSERT(JStr_valid(this));
}


Size JStr_RemoveFront(JStr* this, Size count)
{

	ASSERT(JStr_valid(this));
}

Size JStr_RemoveEnd(JStr* this, Size count)
{

	ASSERT(JStr_valid(this));
}

Size JStr_Sub(JStr* this, Size offset, Size count)
{

	ASSERT(JStr_valid(this));
}

Size JStr_Trim(JStr* this)
{

	ASSERT(JStr_valid(this));
}

Size JStr_RTrim(JStr* this)
{

	ASSERT(JStr_valid(this));
}

Size JStr_LTrim(JStr* this)
{

	ASSERT(JStr_valid(this));
}


Size JStr_CopyToJSB(const JStr* this, JSBuf* jsb)
{

	ASSERT(JStr_valid(this));
}


Bool JStr_MatchStr(const JStr* lhs, const JStr* rhs)
{

	ASSERT(JStr_valid(lhs));
}

Bool JStr_MatchJSB(const JStr* this, const JSBuf* jsb)
{

	ASSERT(JStr_valid(this));
}

Bool JStr_MatchCStr(const JStr* this, Size length, const Char text[PARAM_ELEMENTS(length)])
{

	ASSERT(JStr_valid(this));
}


Bool JStr_StartsWithStr(const JStr* this, const JStr* needle)
{

	ASSERT(JStr_valid(this));
}

Bool JStr_StartsWithChar(const JStr* this, Char n)
{

	ASSERT(JStr_valid(this));
}

Bool JStr_StartsWithJSB(const JStr* this, const JSBuf* needle)
{

	ASSERT(JStr_valid(this));
}

Bool JStr_StartsWithCStr(const JStr* this, Size length, const Char text[PARAM_ELEMENTS(length)])
{

	ASSERT(JStr_valid(this));
}

Bool JStr_EndsWithStr(const JStr* this, const JStr* needle)
{

	ASSERT(JStr_valid(this));
}

Bool JStr_EndsWithChar(const JStr* this, Char n)
{

	ASSERT(JStr_valid(this));
}

Bool JStr_EndsWithJSB(const JStr* this, const JSBuf* needle)
{
	ASSERT(JStr_valid(this));

}

Bool JStr_EndsWithCStr(const JStr* this, Size length, const Char text[PARAM_ELEMENTS(length)])
{
	ASSERT(JStr_valid(this));

}

#define JSTR_ENDSWITHCONSTSTR(js, string) JStr_EndsWithCStr(js, sizeof(string)-1, string)


Bool JStr_ContainsStr(const JStr* this, const JStr* needle)
{
	ASSERT(JStr_valid(this));
	ASSERT(JStr_valid(needle));
}

Bool JStr_ContainsChar(const JStr* this, Char n)
{
	ASSERT(JStr_valid(this));

}

Bool JStr_ContainsJSB(const JStr* this, const JSBuf* needle)
{
	ASSERT(JStr_valid(this));
	ASSERT(JStr_valid(needle));

}

Bool JStr_ContainsCStr(const JStr* this, Size length, const Char text[PARAM_ELEMENTS(length)])
{
	ASSERT(JStr_valid(this));

}

#define JSTR_CONTAINSCONSTSTR(js, string) JStr_EndsWithCStr(js, sizeof(string)-1, string)


Size JStr_FindFirst(const JStr* this, const JStr* needle, Size start)
{
	ASSERT(JStr_valid(this));
	ASSERT(JStr_valid(needle));

}

Size JStr_FindFirstChar(const JStr* this, Char n, Size start)
{
	ASSERT(JStr_valid(this));

}


Size JStr_FindLast(const JStr* this, const JStr* needle, Size start)
{
	ASSERT(JStr_valid(this));
	ASSERT(JStr_valid(needle));

}

Size JStr_FindLastChar(const JStr* this, Char n, Size start)
{
	ASSERT(JStr_valid(this));

}


Size JStr_FindFirstOf(const JStr* this, const JStr* whitelist, Size start)
{
	ASSERT(JStr_valid(this));
	ASSERT(JStr_valid(whitelist));

}

Size JStr_FindFirstNotOf(const JStr* this, const JStr* blacklist, Size start)
{
	ASSERT(JStr_valid(this));
	ASSERT(JStr_valid(blacklist));

}

Size JStr_FindLastOf(const JStr* this, const JStr* whitelist, Size end)
{
	ASSERT(JStr_valid(this));
	ASSERT(JStr_valid(whitelist));

}

Size JStr_FindLastNotOf(const JStr* this, JStr* blacklist, Size end)
{
	ASSERT(JStr_valid(this));
	ASSERT(JStr_valid(blacklist));

}


F32 JStr_ParseF32(const JStr* str)
{
	ASSERT(JStr_valid(str));
}

F64 JStr_ParseF64(const JStr* str)
{
	ASSERT(JStr_valid(str));

}

I32 JStr_ParseI32(const JStr* str, U8 base)
{
	ASSERT(JStr_valid(str));

}

U32 JStr_ParseU32(const JStr* str, U8 base)
{
	ASSERT(JStr_valid(str));

}

I64 JStr_ParseI64(const JStr* str, U8 base)
{
	ASSERT(JStr_valid(str));

}

U64 JStr_ParseU64(const JStr* str, U8 base)
{
	ASSERT(JStr_valid(str));

}

