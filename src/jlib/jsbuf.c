#include "jsbuf.h"

enum JSBUF_CONSTANTS
{
	JSBUF_DEFAULT_CAPACITY = 16, // Default amount of chars allocated
	JSBUF_MAX_BYTES = 100 * 1024 * 1024, // 100 MB sane limit?
};

struct JSBuf
{
	Size length, capacity, cursor;
	Char* data;
};

JSBuf* JSBuf_New(void)
{
	return JSBuf_NewCapacity(JSBUF_DEFAULT_CAPACITY);
}

JSBuf* JSBuf_NewCapacity(Size capacity)
{
	JSBuf* this = JNEW(this);
	this->cursor = this->length = 0;
	this->capacity = capacity;
	this->data = JNEWLIST_FAST(capacity, Char);
	return this;
}

JSBuf* JSBuf_NewArray(Size length, const Char msg[PARAM_ELEMENTS(length)])
{
	ASSERT(length > 0);
	ASSERT(msg);

	JSBuf* this = JSBuf_NewCapacity(length);
	this->length = length;
	for (Size i = 0; i < length; ++i)
		this->data[i] = msg[i];

	return this;
}

JSBuf* JSBuf_NewSubstr(const JSBuf* src, Size offset, Size length)
{
	ASSERT(src);
	ASSERT(offset < src->length);
	ASSERT(length > 0);
	ASSERT(offset+length < src->length);

	return JSBuf_NewArray(length, &src->data[offset]);
}

JSBuf* JSBuf_CopyNew(const JSBuf* src)
{
	ASSERT(src);
	ASSERT(src->data);

	JSBuf* this = JSBuf_NewArray(src->capacity, src->data);
	this->length = src->length;
	this->cursor = src->cursor;

	return this;
}

void JSBuf_Delete(JSBuf** this_p)
{
	ASSERT(this_p);
	JSBuf* this = *this_p;
	ASSERT(this);
	JDELETE(&this->data);
	JDELETE(&this);
	this_p = 0;
}

Bool JSBuf_IsEmpty(const JSBuf* this)
{
	ASSERT(this);
	return JSBuf_Length(this) == 0;
}

Size JSBuf_Length(const JSBuf* this)
{
	ASSERT(this);
	return this->length;
}

void JSBuf_Clear(JSBuf* this, Bool reset_contents)
{
	ASSERT(this);
	this->length = 0;

	if (reset_contents)
		for (Size i = 0; i < this->capacity; ++i)
			this->data[i] = '\0';
}

Size JSBuf_Capacity(const JSBuf* this)
{
	ASSERT(this);
	return this->capacity;
}

Char JSBuf_At(const JSBuf* this, Size index)
{
	ASSERT(this);
	ASSERT(this->length > 0);
	ASSERT(index < this->length);

	return this->data[index];
}

Char JSBuf_AtStart(const JSBuf* this)
{
	ASSERT(this);
	ASSERT(this->length > 0);

	return this->data[0];
}

Char JSBuf_AtEnd(const JSBuf* this)
{
	ASSERT(this);
	ASSERT(this->length > 0);

	return this->data[this->length - 1];
}

const Char* JSBuf_Data(const JSBuf* this)
{
	ASSERT(this);
	ASSERT(this->capacity > 0);
	ASSERT(this->length > 0);

	return this->data;
}

// when stdc_bit_ceil/c23 support is finally added, this should error and be replaced!
FUNC_PRIVATE Size stdc_bit_ceil(Size v)
{
	return 1ULL << ((sizeof(Size)*CHAR_BIT) - __builtin_clzll(v-1));
}

Size JSBuf_ForceResize(JSBuf* this, Size requested)
{
	ASSERT(this);
	ASSERT(requested > 0);

	Size new_capacity = stdc_bit_ceil(requested);
	if (new_capacity == this->capacity)
		return new_capacity;

	ASSERT(new_capacity*sizeof(Char) < JSBUF_MAX_BYTES);
	this->data = JRESIZE(this->data, new_capacity);
	this->capacity = new_capacity;

	if (new_capacity < this->length)
		this->length = this->capacity;

	return this->capacity;
}

Bool JSBuf_Reserve(JSBuf* this, Size requested)
{
	ASSERT(this);
	ASSERT(requested > 0);
	ASSERT(requested*sizeof(Char) < JSBUF_MAX_BYTES);

	if (this->capacity >= requested)
		return false;

	Size new_capacity = JSBuf_ForceResize(this, requested);
	ASSERT(new_capacity >= requested);
	return true;
}

FUNC_PRIVATE void JSBuf_ensurecapacity(JSBuf* this, Size requested)
{
	ASSERT(this);
	ASSERT(requested*sizeof(Char) < JSBUF_MAX_BYTES);

	if (this->capacity < requested)
	{
		Size new_cap = stdc_bit_ceil(requested);
		ASSERT(new_cap * sizeof(Char) < JSBUF_MAX_BYTES);

		JSBuf_ForceResize(this, new_cap);
	}
}

Bool JSBuf_ShrinkToFit(JSBuf* this)
{
	ASSERT(this);
	ASSERT(this->length > 0);
	ASSERT(this->length <= this->capacity);

	// can shrink no more, so bail
	if (this->length == this->capacity)
		return false;

	Size current = this->capacity;
	Size after = JSBuf_ForceResize(this, this->length);

	return current != after;
}

Size JSBuf_SetToChar(JSBuf* this, Size num, Char ch)
{
	ASSERT(this);

	this->length = 0;
	Size new_length = JSBuf_AppendChar(this, num, ch);
	ASSERT(new_length == num);

	return this->length;
}

Size JSBuf_SetToCStr(JSBuf* this, Size length, const Char text[PARAM_ELEMENTS(length)])
{
	ASSERT(this);

	this->length = 0;
	Size new_length = JSBuf_AppendCStr(this, length, text);
	ASSERT(new_length == length);

	return this->length;
}

Size JSBuf_AppendChar(JSBuf* this, Size num, Char ch)
{
	ASSERT(this);
	ASSERT(num > 0);

	Size required_cap = this->length + num;
	JSBuf_ensurecapacity(this, required_cap);

	for (Size i = 0; i < num; ++i)
		this->data[i+this->length] = ch;

	this->length += num;

	return this->length;
}

Size JSBuf_AppendCStr(JSBuf* this, Size length, const Char text[PARAM_ELEMENTS(length)])
{
	ASSERT(this);
	ASSERT(text);
	ASSERT(length > 0);

	Size required_cap = this->length + length;
	JSBuf_ensurecapacity(this, required_cap);

	for (Size i = 0; i < length; ++i)
		this->data[this->length + i] = text[i];

	this->length += length;

	return this->length;
}

Size JSBuf_InsertChar(JSBuf* this, Size num, Char ch, Size offset)
{
	ASSERT(this);
	ASSERT(num > 0);
	ASSERT(offset <= this->length);

	if (offset == this->length)
		return JSBuf_AppendChar(this, num, ch);

	JSBuf_ensurecapacity(this, this->length + num);

	for (Size i = this->length - offset; i > 0; --i)
		this->data[offset + i + num - 1] = this->data[offset + i - 1];
	for (Size i = 0; i < num; ++i)
		this->data[offset + i] = ch;

	this->length += num;
	return this->length;
}

Size JSBuf_InsertCStr(JSBuf* this, Size length, const Char text[PARAM_ELEMENTS(length)], Size offset)
{
	ASSERT(this);
	ASSERT(length > 0);
	ASSERT(offset <= this->length);

	if (offset == this->length)
		return JSBuf_AppendCStr(this, length, text);

	JSBuf_ensurecapacity(this, this->length + length);

	for (Size i = this->length - offset; i > 0; --i)
		this->data[offset + i + length - 1] = this->data[offset + i - 1];
	for (Size i = 0; i < length; ++i)
		this->data[offset + i] = text[i];

	this->length += length;
	return this->length;
}

Size JSBuf_PrependChar(JSBuf* this, Size num, Char ch)
{
	ASSERT(this);

	return JSBuf_InsertChar(this, num, ch, 0);
}

Size JSBuf_PrependCStr(JSBuf* this, Size length, const Char text[PARAM_ELEMENTS(length)])
{
	ASSERT(this);
	ASSERT(length);

	return JSBuf_InsertCStr(this, length, text, 0);
}

void JSBuf_PushBack(JSBuf* this, Char ch)
{
	ASSERT(this);

	JSBuf_AppendChar(this, 1, ch);
}

void JSBuf_PushFront(JSBuf* this, Char ch)
{
	ASSERT(this);
	JSBuf_PrependChar(this, 1, ch);
}

Char JSBuf_PopBack(JSBuf* this)
{
	ASSERT(this);
	ASSERT(this->length > 0);

	this->length--;

	return this->data[this->length];
}

Char JSBuf_PopFront(JSBuf* this)
{
	ASSERT(this);
	ASSERT(this->length > 0);

	Char result = this->data[0];

	for (Size i = 0; i < this->length; ++i)
		this->data[i] = this->data[i+1];
	this->length--;

	return result;
}

Size JSBuf_EraseAt(JSBuf* this, Size offset)
{
	ASSERT(this);
	ASSERT(offset < this->length);

	for (Size i = offset; i < this->length; ++i)
		this->data[i] = this->data[i+1];
	this->length--;

	return this->length;
}

Size JSBuf_EraseCount(JSBuf* this, Size offset, Size count)
{
	ASSERT(this);
	ASSERT(count > 0);
	ASSERT(offset < this->length);
	ASSERT((offset + count-1) <= this->length);

	this->length -= count;
	for (Size i = offset; i < this->length; ++i)
		this->data[i] = this->data[i + count];

	return this->length;
}

Size JSBuf_EraseRange(JSBuf* this, Size start, Size end)
{
	ASSERT(this);
	ASSERT(end > start);
	ASSERT((start+end-1) < this->length);

	return JSBuf_EraseCount(this, start, end-start);
}
