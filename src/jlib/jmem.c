#include "jmem.h"
#include <stdlib.h>

enum JMem_Constants { 
	JMEM_MAX_ALLOC = 100*1024*1024,
};

void* JMem_Alloc(Size element_count, Size entry_size, bool zeroed, ALLOC_MARK_PARAMS)
{
	Size num_bytes = element_count * entry_size;
	ASSERT(num_bytes < JMEM_MAX_ALLOC);

	void* ptr = zeroed ? calloc(element_count, entry_size) : malloc(num_bytes);
	ASSERT(ptr);

	if (!ptr)
		JLog_Log(LOG_PANIC, "Memory allocation failed!", ALLOC_MARK_VARS);

	return ptr;
}

void* JMem_Resize(void* ptr, Size new_bytes, ALLOC_MARK_PARAMS)
{
	ASSERT(new_bytes < JMEM_MAX_ALLOC);
	
	ptr = realloc(ptr, new_bytes);
	ASSERT(ptr);
	if (!ptr)
		JLog_Log(LOG_PANIC, "Memory re-allocation failed!", ALLOC_MARK_VARS);

	return ptr;
}

void JMem_Delete(void** ptr_p, ALLOC_MARK_PARAMS)
{
	ASSERT(ptr_p);
	void* ptr = *ptr_p;
	ASSERT(ptr);

	free(ptr);
	ptr = 0;
	ptr_p = 0;
}
