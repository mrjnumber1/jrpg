#include "jsys.h"
#include <stdio.h>
#include <stdlib.h>

void JSys_Puts(Size length, const Char msg[PARAM_ELEMENTS(length)])
{
	ASSERT(length != 0);
	size_t written = fwrite(msg, length, 1, stdout);
	ASSERT(written == length);
	int rc = fputc('\n', stdout);	
	ASSERT(rc != EOF);
}

FUNCATTR_NORETURN void JSys_Abort(void)
{
	DEBUGTRAP;
	abort();
	ASSERT(false);
	while (1);
}
