#include "jlog.h"
#include <stdio.h>
#include <string.h>
#include <time.h>

struct { Char const* const Full; Char const Initial; } level_names[LOG_DISABLE] = 
{
	[LOG_TRACE] = {"TRACE", 'T'},
	[LOG_DEBUG] = {"DEBUG", 'D'},
	[LOG_INFO] = {"INFO", 'I'},
	[LOG_WARN] = {"WARN", 'W'},
	[LOG_ERROR] = {"ERROR", 'E'},
	[LOG_PANIC] = {"PANIC", 'P'},
};

void JLog_Log(JLogLevel level, const Char msg[], ALLOC_MARK_PARAMS)
{
	ASSERT(msg);
	ASSERT(level != LOG_DISABLE);

	const time_t now = time(0);
	ASSERT(now != (time_t)-1);

	Char timestr[sizeof("23:59:59")];
	Size len = strftime(timestr, sizeof(timestr), "%H:%M:%S", localtime(&now));
	ASSERT(len != 0);
	timestr[len] = '\0';

	Char output[1*512];
	memset(output, '\0', sizeof(output));
	len = snprintf(output, sizeof(output), "[%s]%c@%s:%llu(%s) > ", timestr, level_names[level].Initial, file, line, func);
	ASSERT(len < sizeof(output));
	memcpy(output+len, msg, sizeof(output) - len); 

	FILE* stream = (level < LOG_ERROR) ? stdout : stderr;
	int rc = fputs(output, stream); 
	ASSERT(rc != EOF);
	rc = fputc('\n', stream);
	ASSERT(rc != EOF);

	if (level == LOG_PANIC)
		JSys_Abort();

}

