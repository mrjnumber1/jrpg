.POSIX:
SHELL = /bin/sh
#general defines for the environment
USE_DEBUG = true
USE_WINDOWS = true
# USE_WINDOWS_SHELL = true 

# defines for the actual program and where it outputs
MAINLIB = jlib
SELFTEST = test
PROGRAMS = jcreate jplay $(SELFTEST) 
TARGETS = $(MAINLIB) $(PROGRAMS)
DIR_OUT = build
DIR_LIB = $(DIR_OUT)/lib
DIR_BIN = $(DIR_OUT)/bin

# Defines for the compiler/linker
CC = gcc
CSTD = c2x

# CFLAGS defines
# CFLAGS for all targets
CFLAGS = -std=$(CSTD)
# CFLAGS_<target> define CFLAGS for <target> 

# Defines for all targets (will be prefixed with -D)
CFLAGS_DEFINE = NDEBUG 
# CFLAGS_DEFINE_<target> define values for <target>

# Warnings for all targets (will be prefixed with -W)
CFLAGS_WARN = all error conversion double-promotion no-unused-parameter no-unused-function no-sign-conversion
# CFLAGS_WARN_<target> add warnings for <target> (will be prefixed with -W) 

# Include directories for all targets (will be prefixed with -I)
CFLAGS_DIRS = include/$(MAINLIB)
# CFLAGS_DIRS_<target> add dirs for <target> (will be prefixed with -I)

# LDFLAGS defines
# LDFLAGS for all targets
# LDFLAGS =
# LDFLAGS_<target> add LDFLAGS to <target>

# Directories with libs for all targets (list will be prefixed with -L)
LDFLAGS_DIRS = $(DIR_LIB)
# LDFLAGS_DIRS_<target> Directories with libs for <target> (list will be prefixed with -L)

# Libs for all targets (list will be prefixed with -l) 
LDLIBS = $(MAINLIB)
# LDLIBS_<target> add libs to <target> (list will be prefixed with -l)
LDLIBS_$(SELFTEST) = check

RM = rm -rf
MKDIR = mkdir -p
ERROR_TO_NULL = 

ifdef USE_DEBUG
	CFLAGS += -ggdb3
	CFLAGS_DEFINE := $(filter-out NDEBUG, $(CFLAGS_DEFINE)) 
endif
ifdef USE_SANITIZE
	CFLAGS += -fsanitize=address,undefined -fsanitize-trap=all
endif

ifdef USE_WINDOWS
	EXE = .exe
	SLIB = .lib
	DLIB = .dll
endif
ifdef USE_WINDOWS_SHELL
	RM = rd /Q /S
	MKDIR = mkdir
	ERROR_TO_NULL = 2>NUL
endif

define OBJECTS_template =
OBJ_$(1) := $$(patsubst src/%, $$(DIR_OUT)/%, $$(patsubst %.c, %.o, $$(wildcard src/$(1)/*.c)))
endef

define COMPILAND_template =
$$(DIR_OUT)/$(1)/%.o: src/$(1)/%.c
	$$(CC) $$(CFLAGS) $$(CFLAGS_$(1)) $$(addprefix -D, $$(CFLAGS_DEFINE) $$(CFLAGS_DEFINE_$(1))) $$(addprefix -I, $$(CFLAGS_DIRS) $$(CFLAGS_DIRS_$(1)) $$(if $$(filter-out %$$(MAINLIB),$(1)), include/$(1))) $$(addprefix -W, $$(CFLAGS_WARN) $$(CFLAGS_WARN_$(1))) -o $$@ -c $$<
endef

define PROGRAM_template = 
$(1): dirs $$(MAINLIB) $$(OBJ_$(1))
	$$(CC) -o $$(DIR_BIN)/$$@$$(EXE) $$(LDFLAGS) $$(LDFLAGS_$(1)) $$(addprefix -L, $$(LDFLAGS_DIRS) $$(LDFLAGS_DIRS_$(1))) $$(OBJ_$(1)) $$(addprefix -l, $$(LDLIBS) $$(LDLIBS_$(1)))
endef

$(foreach objlist, $(TARGETS), $(eval $(call OBJECTS_template,$(objlist))))
$(foreach compiland, $(TARGETS), $(eval $(call COMPILAND_template,$(compiland))))
$(foreach program, $(PROGRAMS), $(eval $(call PROGRAM_template,$(program))))


# Actual make targets... and it only took ~100 lines!
all: dirs $(TARGETS) 
clean:
	$(RM) $(DIR_OUT)
mostlyclean:
	$(RM) $(addprefix $(DIR_OUT)/,$(PROGRAMS))
dirs:
	$(MKDIR) $(DIR_OUT) $(DIR_LIB) $(DIR_BIN) $(addprefix $(DIR_OUT)/,$(TARGETS)) $(ERROR_TO_NULL)

$(MAINLIB): dirs $(OBJ_$(MAINLIB))
	$(AR) rcs $(DIR_LIB)/$@$(SLIB) $(OBJ_$(MAINLIB))

check: $(SELFTEST)
	$(DIR_BIN)/$(SELFTEST)$(EXE)

